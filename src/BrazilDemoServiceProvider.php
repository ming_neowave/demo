<?php

namespace Brazil\Demo;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Brazil\Demo\Command\DemoMigration;
class BrazilDemoServiceProvider extends ServiceProvider
{
	protected $namespace = 'Brazil\Demo\Http';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $this->bootCommands();
    }

    
    /**
     * 
     * @param  Router $router 
     * @return 
     */
    public function map(Router $router)
    {
    	$router->group(['prefix' => 'demo'],function() use ($router){

    		$router->get('/hello',function(){

	    		return 'hehe';

	    	});
    	});
    	
    	// if (! $this->app->routesAreCached()) {
	    //     require __DIR__.'/../../routes.php';
	    // }

    }

    protected function bootCommands(){
    	$this->app['brazil.command.migrate'] = $this->app->share(function($app)
		{
			return new DemoMigration;
		});

		$this->commands('brazil.command.migrate');
    }
}
