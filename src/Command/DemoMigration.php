<?php

namespace Brazil\Demo\Command;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class DemoMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'brazil:core.migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'demo migration';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // dd(dirname(__DIR__).'/Migration');
        $this->call('migrate', array('--path' => 'packages/brazil/demo/src/Migration/'));
    }
    // public function handle()
    // {
    //     $this->app
    //     $this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
    // }
}
